package occonf

import (
	"errors"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/goccy/go-yaml"
)

type ConfigFormat string

const (
	FormatV2 ConfigFormat = "oc2"
	FormatV3 ConfigFormat = "oc3"
)

type ConfigFile struct {
	Path string `yaml:"-"`

	Reason                *ConfigReason           `yaml:"reason,omitempty"`
	IndexFields           []IndexField            `yaml:"indexfields"`
	ContentTypes          []map[string][]Property `yaml:"contenttypes"`
	Extractors            []Extractor             `yaml:"xpathextractors"`
	XPathNamespaces       []XPathNamespace        `yaml:"xpathnamespaces"`
	XSLTExtractors        []XSLTExtractor         `yaml:"xsltextractors"`
	RelationExtractors    []RelationExtractor     `yaml:"relationextractors"`
	Groups                []string                `yaml:"groups"`
	Paths                 []PathPattern           `yaml:"paths,omitempty"` // Borta i OC3
	GroupRules            []GroupRule             `yaml:"importgrouprules"`
	Relations             []Relation              `yaml:"relations"`
	Permissions           []Permission            `yaml:"permissions"`
	ContentTypeRules      []ContentTypeRule       `yaml:"importcontenttyperules"`
	StorageRule           []StorageRule           `yaml:"importstoragerules,omitempty"` // Borta i OC3
	Versioning            *Versioning             `yaml:"versioning,omitempty"`         // Borta i OC3
	Sorting               []Sorting               `yaml:"sorting,omitempty"`            // Borta i OC3
	DateAndTime           *DateAndTime            `yaml:"dateandtime,omitempty"`
	Faceting              *Faceting               `yaml:"faceting,omitempty"`              // Borta i OC3
	DefaultSearchResponse interface{}             `yaml:"defaultsearchresponse,omitempty"` // Ignorera
}

func (c *ConfigFile) WriteToFile(path string) (err error) {
	f, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("failed to create file: %w", err)
	}

	defer func() {
		cErr := f.Close()
		if cErr != nil && err == nil {
			err = fmt.Errorf("failed to close file: %w", err)
		}
	}()

	err = yaml.NewEncoder(f).Encode(c)
	if err != nil {
		return fmt.Errorf("failed to encode config: %w", err)
	}

	return nil
}

func (c *ConfigFile) Sort() {
	sort.Slice(c.IndexFields, func(i, j int) bool {
		return strings.Compare(
			c.IndexFields[i].Name,
			c.IndexFields[j].Name,
		) < 0
	})

	sort.Slice(c.ContentTypes, func(i, j int) bool {
		return strings.Compare(
			contentTypeKey(c.ContentTypes[i]),
			contentTypeKey(c.ContentTypes[j]),
		) < 0
	})

	for i := range c.ContentTypes {
		for t := range c.ContentTypes[i] {
			propList := c.ContentTypes[i][t]
			sort.Slice(propList, func(i, j int) bool {
				return strings.Compare(
					propList[i].Name,
					propList[j].Name,
				) < 0
			})
		}
	}

	sort.Slice(c.Extractors, func(i, j int) bool {
		return strings.Compare(
			c.Extractors[i].ID,
			c.Extractors[j].ID,
		) < 0
	})

	sort.Slice(c.XPathNamespaces, func(i, j int) bool {
		return strings.Compare(
			c.XPathNamespaces[i].Prefix,
			c.XPathNamespaces[j].Prefix,
		) < 0
	})

	sort.Slice(c.XSLTExtractors, func(i, j int) bool {
		return strings.Compare(
			c.XSLTExtractors[i].ID,
			c.XSLTExtractors[j].ID,
		) < 0
	})

	sort.Slice(c.RelationExtractors, func(i, j int) bool {
		return strings.Compare(
			c.RelationExtractors[i].ID,
			c.RelationExtractors[j].ID,
		) < 0
	})

	sort.Strings(c.Groups)

	sort.Slice(c.Permissions, func(i, j int) bool {
		return strings.Compare(
			c.Permissions[i].Key(),
			c.Permissions[j].Key(),
		) < 0
	})

	sort.Slice(c.GroupRules, func(i, j int) bool {
		return strings.Compare(
			c.GroupRules[i].ID,
			c.GroupRules[j].ID,
		) < 0
	})

	sort.Slice(c.ContentTypeRules, func(i, j int) bool {
		return strings.Compare(
			c.ContentTypeRules[i].ID,
			c.ContentTypeRules[j].ID,
		) < 0
	})

	sort.Slice(c.StorageRule, func(i, j int) bool {
		return strings.Compare(
			c.StorageRule[i].ID,
			c.GroupRules[j].ID,
		) < 0
	})

	sort.Slice(c.Paths, func(i, j int) bool {
		return strings.Compare(
			c.Paths[i].ContentType,
			c.Paths[j].ContentType,
		) < 0
	})

	sort.Slice(c.Sorting, func(i, j int) bool {
		return strings.Compare(
			c.Sorting[i].Key(),
			c.Sorting[j].Key(),
		) < 0
	})
}

func contentTypeKey(m map[string][]Property) string {
	for k := range m {
		return k
	}
	return ""
}

type DateAndTime struct {
	File string `yaml:"-"`

	TimeZone string `yaml:"timezone"`
}

func (dt DateAndTime) SourcePath() string {
	return dt.File
}

func (dt DateAndTime) Identity() string {
	return "date and time settings"
}

type XPathNamespace struct {
	File      string `yaml:"-"`
	MimeType  string `yaml:"mimetype"`
	Namespace string `yaml:"namespace"`
	Prefix    string `yaml:"prefix"`
}

func (n XPathNamespace) Identity() string {
	return n.Prefix + " namespace"
}

func (n XPathNamespace) SourcePath() string {
	return n.File
}

type PathPattern struct {
	File        string `yaml:"-"`
	ContentType string `yaml:"contenttype,omitempty"`
	Pattern     string `yaml:"pattern,omitempty"`
	Fallback    string `yaml:"fallback,omitempty"`
}

func (p PathPattern) Identity() string {
	pType := "default"
	if p.ContentType != "" {
		pType = p.ContentType
	}
	return pType + " path pattern"
}

func (p PathPattern) SourcePath() string {
	return p.File
}

type Faceting struct {
	File string `yaml:"-"`

	Enabled       bool                  `yaml:"enabled"`
	MaxFields     int                   `yaml:"maxfacetfields"`
	MinCount      int                   `yaml:"minfacetcount"`
	MinDateCount  int                   `yaml:"mindatefacetcount"`
	DefaultFields []string              `yaml:"defaultfields,omitempty"`
	ContentTypes  []FacetingContentType `yaml:"contenttypes,omitempty"`
}

func (f Faceting) SourcePath() string {
	return f.File
}

func (f Faceting) Identity() string {
	return "faceting settings"
}

type FacetingContentType struct {
	Name   string   `yaml:"name"`
	Fields []string `yaml:"indexfields"`
}

type ConfigReason struct {
	Reason    string    `yaml:"reason"`
	Name      string    `yaml:"name"`
	Activated time.Time `yaml:"activated"`
}

type IndexField struct {
	File string `yaml:"-"`

	Name        string `yaml:"name"`
	Type        string `yaml:"type"`
	Description string `yaml:"description"`
	Multivalued bool   `yaml:"multivalued"`
	Sortable    bool   `yaml:"sortable"`
	ReadOnly    bool   `yaml:"readonly"`
	Suggest     bool   `yaml:"suggest"`
}

var fieldTypes = map[string]bool{
	"STRING":           true,
	"DATE":             true,
	"TEXT":             true,
	"STRING_LOWERCASE": true,
	"BOOLEAN":          true,
	"GEO":              true,
	"INTEGER":          true,
	"LONG":             true,
	"DOUBLE":           true,
}

func (i IndexField) Validate(conf *Config) error {
	if err := validateName(i.Name); err != nil {
		return err
	}

	if err := validateDescription(i.Description); err != nil {
		return err
	}

	ok := fieldTypes[i.Type]
	if !ok {
		return fmt.Errorf("invalid type %q", i.Type)
	}

	return nil
}

func validateName(name string) error {
	if name == "" {
		return errors.New("missing name")
	}

	upper := strings.ToUpper(name)
	if upper[0] != name[0] {
		return fmt.Errorf("name %q does not begin with an uppercase character", name)
	}

	for _, c := range name {
		if (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') {
			continue
		}

		return fmt.Errorf("names may not contain %q", string(c))
	}

	return nil
}

var descRunes = map[rune]bool{
	'.': true,
	',': true,
	' ': true,
}

func validateDescription(desc string) error {
	for _, c := range desc {
		if (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || descRunes[c] {
			continue
		}

		return fmt.Errorf("descriptions may not contain %q", string(c))
	}

	return nil
}

func (i IndexField) SourcePath() string {
	return i.File
}

func (i IndexField) Identity() string {
	return fmt.Sprintf("index field %s", i.Name)
}

type Property struct {
	File string `yaml:"-"`

	ContentType string   `yaml:"-"`
	Name        string   `yaml:"name"`
	Type        string   `yaml:"type"`
	Description string   `yaml:"description"`
	Multivalued bool     `yaml:"multivalued"`
	Sortable    bool     `yaml:"sortable"`
	Searchable  bool     `yaml:"searchable"`
	ReadOnly    bool     `yaml:"readonly"`
	Identifier  bool     `yaml:"identifier"`
	IndexFields []string `yaml:"indexfields,omitempty"`
}

var propertyTypes = map[string][]string{
	"STRING":  {"STRING", "TEXT", "STRING_LOWERCASE"},
	"DATE":    {"DATE"},
	"BOOLEAN": {"BOOLEAN"},
	"WKT":     {"GEO"},
	"INTEGER": {"INTEGER"},
	"LONG":    {"LONG"},
	"DOUBLE":  {"DOUBLE"},
}

func (p Property) Validate(conf *Config) error {
	if err := validateName(p.Name); err != nil {
		return err
	}

	if err := validateDescription(p.Description); err != nil {
		return err
	}

	fieldTypes, isBuiltin := propertyTypes[p.Type]

	if isBuiltin {
		if len(p.IndexFields) == 0 {
			return errors.New("missing index field")
		}

		if len(p.IndexFields) != 1 {
			return errors.New("only one index field can be specified")
		}
	}

	if !isBuiltin {
		if len(p.IndexFields) > 0 {
			return errors.New(
				"index fields can only be specified for non-relationship properties")
		}
	}

	if conf != nil {
		_, isTypeRef := conf.ContentTypes[p.Type]

		if !isTypeRef && !isBuiltin {
			return fmt.Errorf("invalid type %q", p.Type)
		}

		relNotation := !strings.HasSuffix(p.Name, "Relations") ||
			!strings.HasSuffix(p.Name, "Relation")

		if !isBuiltin && !relNotation {
			conf.warningInFile(p.File, fmt.Errorf(
				`%s: relationship properties should end with "Relation[s]"`,
				p.Identity(),
			))
		}

		if isBuiltin {
			if p.IndexFields[0] != p.Name {
				return fmt.Errorf(
					"the property must have the same name as the index field, wanted %q got %q",
					p.Name, p.IndexFields[0],
				)
			}

			field, ok := conf.IndexFields[p.IndexFields[0]]
			if !ok {
				return fmt.Errorf("missing index field %q",
					p.IndexFields[0])
			}

			if conf.checks[CheckHasExtractor] {
				hasExtractor := checkForExtractor(p.ContentType, p.Name, conf)
				if !hasExtractor {
					return fmt.Errorf("missing extractor that targets the property")
				}
			}

			if !stringSliceContains(fieldTypes, field.Type) {
				return fmt.Errorf("index field type mismatch, wanted one of %s, got %q",
					strings.Join(fieldTypes, ", "), field.Type)
			}

			if field.Multivalued != p.Multivalued {
				return fmt.Errorf("multivalued mismatch, index field was set to %v, expected %v",
					field.Multivalued, p.Multivalued)
			}
		}
	}

	return nil
}

func checkForExtractor(contentType, property string, conf *Config) bool {
	for _, ex := range conf.Extractors {
		if ex.ContentType == contentType && ex.Property == property {
			return true
		}
	}

	for _, ex := range conf.XSLTExtractors {
		if ex.ContentType == contentType && ex.Property == property {
			return true
		}
	}

	return false
}

func stringSliceContains(s []string, str string) bool {
	for i := range s {
		if s[i] == str {
			return true
		}
	}
	return false
}

func (p Property) SourcePath() string {
	return p.File
}

func (p Property) Identity() string {
	return fmt.Sprintf("property %s.%s", p.ContentType, p.Name)
}

type Extractor struct {
	File string `yaml:"-"`

	ID          string `yaml:"id"`
	Property    string `yaml:"property"`
	ContentType string `yaml:"contenttype"`
	MimeType    string `yaml:"mimetype"`
	XPath       string `yaml:"xpath"`
	IgnoreRoot  bool   `yaml:"ignoreroot"`
}

func (e Extractor) SourcePath() string {
	return e.File
}

func (e Extractor) Identity() string {
	return fmt.Sprintf("xpath extractor %s (for %s.%s)",
		e.ID, e.ContentType, e.Property)
}

func (e Extractor) Validate(conf *Config) error {
	if e.ID == "" {
		return errors.New("missing id")
	}

	if e.ContentType == "" {
		return errors.New("missing content type")
	}

	if e.Property == "" {
		return errors.New("missing property")
	}

	if e.XPath == "" {
		return errors.New("missing xpath")
	}

	if conf != nil {
		contentType, ok := conf.ContentTypes[e.ContentType]
		if !ok {
			return fmt.Errorf("unknown content type %q", e.ContentType)
		}

		_, ok = contentType[e.Property]
		if !ok {
			return fmt.Errorf("unknown property %s.%s", e.ContentType, e.Property)
		}
	}

	return nil
}

type XSLTExtractor struct {
	File string `yaml:"-"`

	ID          string `yaml:"id"`
	Property    string `yaml:"property"`
	ContentType string `yaml:"contenttype"`
	MimeType    string `yaml:"mimetype"`
	XSLT        string `yaml:"xslt,flow"`
}

func (e XSLTExtractor) SourcePath() string {
	return e.File
}

func (e XSLTExtractor) Identity() string {
	return fmt.Sprintf("XSLT extractor %s for %s.%s", e.ID, e.ContentType, e.Property)
}

func (e XSLTExtractor) Validate(conf *Config) error {
	if e.ID == "" {
		return errors.New("missing id")
	}

	if e.ContentType == "" {
		return errors.New("missing content type")
	}

	if e.Property == "" {
		return errors.New("missing property")
	}

	if e.XSLT == "" {
		return errors.New("missing XSLT")
	}

	if conf != nil {
		contentType, ok := conf.ContentTypes[e.ContentType]
		if !ok {
			return fmt.Errorf("unknown content type %q", e.ContentType)
		}

		_, ok = contentType[e.Property]
		if !ok {
			return fmt.Errorf("unknown property %s.%s", e.ContentType, e.Property)
		}
	}

	return nil
}

type Relation struct {
	File string `yaml:"-"`

	Name              string               `yaml:"name"`
	SourceContentType string               `yaml:"sourcecontenttype"`
	TargetContentType string               `yaml:"targetcontenttype"`
	Constraints       []RelationConstraint `yaml:"constraints"`
}

func (r Relation) SourcePath() string {
	return r.File
}

func (r Relation) Identity() string {
	return fmt.Sprintf("relation %s from %s to %s",
		r.Name, r.SourceContentType, r.TargetContentType)
}

type RelationExtractor struct {
	File string `yaml:"-"`

	ID                string               `yaml:"id"`
	Property          string               `yaml:"property"`
	ContentType       string               `yaml:"contenttype"`
	TargetContentType string               `yaml:"targetcontenttype"`
	Constraint        *RelationConstraint  `yaml:"constraint,omitempty"`
	Constraints       []RelationConstraint `yaml:"constraints,omitempty"`
}

func (e *RelationExtractor) Normalise(format ConfigFormat) error {
	err := e.Validate(nil)
	if err != nil {
		return err
	}

	switch format {
	case FormatV2:
		if e.Constraint != nil {
			e.Constraints = []RelationConstraint{*e.Constraint}
		}
		e.Constraint = nil
	case FormatV3:
		if len(e.Constraints) > 0 {
			e.Constraint = &e.Constraints[0]
		}
		e.Constraints = nil
	}

	return nil
}

func (e RelationExtractor) Validate(conf *Config) error {
	hasV3 := e.Constraint != nil
	hasV2 := len(e.Constraints) > 0

	if hasV2 && hasV3 {
		return errors.New("contains both V2 and V3 style constraints")
	}

	if hasV2 && len(e.Constraints) > 1 {
		return errors.New("only one constraint is supported")
	}

	if e.ID == "" {
		return errors.New("missing id")
	}

	if e.ContentType == "" {
		return errors.New("missing content type")
	}

	if e.Property == "" {
		return errors.New("missing property")
	}

	if e.TargetContentType == "" {
		return errors.New("missing target content type")
	}

	if conf != nil {
		_, ok := conf.ContentTypes[e.ContentType]
		if !ok {
			return fmt.Errorf("unknown content type %q", e.ContentType)
		}

		_, ok = conf.ContentTypes[e.TargetContentType]
		if !ok {
			return fmt.Errorf("unknown target content type %q", e.ContentType)
		}
	}

	return nil
}

func (e RelationExtractor) SourcePath() string {
	return e.File
}

func (e RelationExtractor) Identity() string {
	return fmt.Sprintf("relation extractor %s for %s.%s", e.ID, e.ContentType, e.Property)
}

type RelationConstraint struct {
	SourceProperty string `yaml:"sourceproperty"`
	TargetProperty string `yaml:"targetproperty"`
}

type Permission struct {
	File string `yaml:"-"`

	Group  string `yaml:"group"`
	Role   string `yaml:"role"`
	View   bool   `yaml:"view"`
	Add    bool   `yaml:"add"`
	Update bool   `yaml:"update"`
	Purge  bool   `yaml:"purge"`
	Delete bool   `yaml:"delete"`
}

func (p Permission) SourcePath() string {
	return p.File
}

func (p Permission) Key() string {
	return fmt.Sprintf("%s.%s", p.Group, p.Role)
}

func (p Permission) Identity() string {
	return fmt.Sprintf("permission for group %s role %s", p.Group, p.Role)
}

type ContentTypeRule struct {
	File string `yaml:"-"`

	ID                string   `yaml:"id"`
	ContentType       string   `yaml:"contenttype"`
	MimeTypes         []string `yaml:"mimetypes,omitempty"`
	MetadataMimeTypes []string `yaml:"metadatamimetypes,omitempty"`
}

func (r ContentTypeRule) SourcePath() string {
	return r.File
}

func (r ContentTypeRule) Identity() string {
	return fmt.Sprintf("content type rule %s", r.ID)
}

func (r ContentTypeRule) Validate(conf *Config) error {
	if r.ID == "" {
		return errors.New("missing id")
	}

	if r.ContentType == "" {
		return errors.New("missing content type")
	}

	if len(r.MimeTypes) == 0 && len(r.MetadataMimeTypes) == 0 {
		return errors.New("no mimetypes specified")
	}

	if conf != nil {
		_, validTypeRef := conf.ContentTypes[r.ContentType]

		if !validTypeRef {
			return fmt.Errorf("invalid content type %q", r.ContentType)
		}
	}

	return nil
}

type StorageRule struct {
	File string `yaml:"-"`

	ID   string `yaml:"id"`
	Disk string `yaml:"disk"`
}

func (r StorageRule) SourcePath() string {
	return r.File
}

func (r StorageRule) Identity() string {
	return fmt.Sprintf("storage rule %s", r.ID)
}

func (r StorageRule) Validate(conf *Config) error {
	if r.ID == "" {
		return errors.New("missing id")
	}

	if r.Disk != "OpenContent" {
		return fmt.Errorf(`invalid disk specifier %q, must be "OpenContent"`, r.Disk)
	}

	return nil
}

type GroupRule struct {
	File string `yaml:"-"`

	ID          string       `yaml:"id"`
	Group       string       `yaml:"group"`
	Expressions []Expression `yaml:"expressions"`
}

func (r GroupRule) SourcePath() string {
	return r.File
}

func (r GroupRule) Identity() string {
	return fmt.Sprintf("group rule %s for %s", r.ID, r.Group)
}

type Expression struct {
	ID          string `yaml:"id"`
	Property    string `yaml:"property"`
	ContentType string `yaml:"contenttype"`
	Operator    string `yaml:"operator"`
	Value       string `yaml:"value"`
}

type Sorting struct {
	File string `yaml:"-"`

	Name        string      `yaml:"name"`
	ContentType string      `yaml:"contenttype,omitempty"`
	Fields      []SortField `yaml:"indexfields"`
}

func (s Sorting) SourcePath() string {
	return s.File
}

func (s Sorting) Key() string {
	return fmt.Sprintf("%s/%s", s.ContentType, s.Name)
}

func (s Sorting) Identity() string {
	return fmt.Sprintf("sorting %s", s.Name)
}

type SortField struct {
	Name      string `yaml:"name"`
	Ascending bool   `yaml:"ascending"`
}

type Versioning struct {
	File string `yaml:"-"`

	KeepOnDelete *bool                   `yaml:"keepondelete,omitempty"`
	ContentTypes []ContentTypeVersioning `yaml:"contenttypes"`
}

func (v Versioning) GetRule(contentType string) (ContentTypeVersioning, int) {
	for i := range v.ContentTypes {
		if v.ContentTypes[i].Name == contentType {
			return v.ContentTypes[i], i
		}
	}

	return ContentTypeVersioning{}, -1
}

func (v *Versioning) Validate(conf *Config) error {
	if v == nil {
		return nil
	}

	for i := range v.ContentTypes {
		err := v.ContentTypes[i].Validate(conf)
		if err != nil {
			return err
		}
	}

	return nil
}

type ContentTypeVersioning struct {
	File string `yaml:"-"`

	Name        string `yaml:"name"`
	MaxVersions int    `yaml:"maxversions"`
}

func (v ContentTypeVersioning) SourcePath() string {
	return v.File
}

func (v ContentTypeVersioning) Identity() string {
	return fmt.Sprintf("versioning rule for %s", v.Name)
}

func (v ContentTypeVersioning) Validate(conf *Config) error {
	if v.Name == "" {
		return errors.New("missing content type")
	}

	if conf != nil {
		_, validTypeRef := conf.ContentTypes[v.Name]

		if !validTypeRef {
			return fmt.Errorf("invalid content type %q", v.Name)
		}
	}

	return nil
}
