package occonf

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/goccy/go-yaml"
)

type MergeOptions struct {
	outputFormat ConfigFormat
	resolver     ConflictResolver
	checks       map[string]bool
}

type MergeOption func(opts *MergeOptions)

func WithMergeOutputFormat(f ConfigFormat) MergeOption {
	return func(opts *MergeOptions) {
		opts.outputFormat = f
	}
}

func WithMergeResolver(r ConflictResolver) MergeOption {
	return func(opts *MergeOptions) {
		opts.resolver = r
	}
}

func WithChecks(checks map[string]bool) MergeOption {
	return func(opts *MergeOptions) {
		opts.checks = checks
	}
}

func MergeFiles(paths []string, options ...MergeOption) (*Config, error) {
	conf := NewConfig()

	opts := MergeOptions{
		outputFormat: FormatV2,
		resolver:     &OverwriteWithWarning{},
	}

	for _, opt := range options {
		opt(&opts)
	}

	// This split between MergeFiles() and Config chafes a bit,
	// might be worth looking at.
	for k, v := range opts.checks {
		conf.checks[k] = v
	}

	err := mergeFiles(conf, opts.resolver, paths)
	if err != nil {
		return nil, err
	}

	return conf, nil
}

func mergeFiles(conf *Config, resolv ConflictResolver, read []string) error {
	walker := mergeWalker(conf, resolv)
	walker = WithFileError(walker)

	for _, path := range read {
		info, err := os.Stat(path)
		if err != nil {
			return fmt.Errorf("failed to stat %q: %w", path, err)
		}

		if info.IsDir() {
			err := filepath.Walk(path, walker)
			if err != nil {
				return fmt.Errorf(
					"failed while processing directory %q: %w",
					path, err,
				)
			}
			continue
		}

		err = readAndMerge(conf, path, resolv)
		if err != nil {
			return fmt.Errorf(
				"failed while processing file %q: %w",
				path, err,
			)
		}
	}

	return nil
}

type FileError struct {
	Path string
	Err  error
}

func (fe *FileError) Unwrap() error {
	return fe.Err
}

func (fe *FileError) Error() string {
	return fmt.Sprintf("failed to handle %q: %v", fe.Path, fe.Err)
}

func WithFileError(fn filepath.WalkFunc) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		outErr := fn(path, info, err)
		if outErr != nil {
			outErr = &FileError{
				Path: path,
				Err:  outErr,
			}
		}
		return outErr
	}
}

func mergeWalker(conf *Config, resolv ConflictResolver) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("failed to walk %q: %v", path, err)
			return nil
		}

		if info.IsDir() {
			return nil
		}

		if !strings.HasSuffix(path, ".yaml") {
			return nil
		}

		base := filepath.Base(path)
		parts := strings.SplitN(base, "-", 2)

		// Check if it's a numbered file
		_, err = strconv.Atoi(parts[0])
		if err != nil {
			return nil
		}

		return readAndMerge(conf, path, resolv)
	}
}

func readAndMerge(conf *Config, path string, resolv ConflictResolver) error {
	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf(
			"failed to open configuration file: %w", err)
	}

	defer func() {
		err := file.Close()
		if err != nil {
			log.Printf("failed to close file %q: %v", path, err)
		}
	}()

	dec := yaml.NewDecoder(file,
		yaml.DisallowDuplicateKey(),
		yaml.DisallowUnknownField(),
	)

	config := ConfigFile{
		Path: path,
	}

	err = dec.Decode(&config)
	if err != nil {
		return fmt.Errorf("failed to decode file: %w", err)
	}

	err = conf.Merge(config, resolv)
	if err != nil {
		return fmt.Errorf("failed to merge config: %w", err)
	}

	return nil
}
