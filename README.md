# OC Configuration tool

`occonf` merges and validates Open Content configuration files.

## Installation

Install through our Homebrew tap https://github.com/Infomaker/homebrew-core or clone and run `go install ./cmd/occonf`.

## Merging

```
NAME:
   occonf merge - merges open content configurations

USAGE:
   occonf merge [command options] [arguments...]

DESCRIPTION:
   takes one or more configuration files and merges and validates them

OPTIONS:
   --format value                The format to use, one of "oc2" or "oc3" (default: "oc2")
   --out value                   The output file, defaults to stdout
   --strategy value              The merge strategy to use, one of "overwrite" and "interactive" (default: "overwrite")
   --auto-merge value, -a value  Automatically apply changes for specific fields when running interactively
   --disable value, -d value     Disable configuration checks, can be specified multiple times, one of: has-extractor
   --enable value, -e value      Enable configuration checks, can be specified multiple times, one of: has-extractor
   --strict                      Treat warnings as errors (default: true)
   --help, -h                    show help (default: false)
```

Example, creating a single editorial config:

```
merge --out editorial.yaml editorial/editorial-base-config.yaml editorial
```

This merges "editorial/editorial-base-config.yaml" and all numbered files in the directory "editorial". An arbitrary number of files can be supplied.

The default strategy is a strict non-interactive merge. All conflicts will be treated as errors. If f.ex. "Description" has changed for a lot of properties, but we want them to be accepted without being treated as a conflict we can add the flag `-a Description` to accept Description conflicts.

The tool will run a integrity check after the merge has been performed. A configuration that fail the integrity check will still be written out. This gives you an opportunity to edit the file, and then run validation again using the "validate" command: `occonf validate -e has-extractor merged.yaml`.

Example, interactively merging a customer config ("oc-exported-active.yaml") with the editorial standard configuration:

```
occonf merge --strategy interactive -a Description -a Sortable -a Suggest --out merged.yaml oc-exported-active.yaml editorial/editorial-base-config.yaml editorial
```

![Screencast](./docs/cast.gif)

Here we automatically accept changes that only affect `Description, Sortable, Suggest`.

