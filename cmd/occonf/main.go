package main

import (
	"os"

	"bitbucket.org/infomaker/occonf"
)

func main() {
	if err := occonf.Run(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}
