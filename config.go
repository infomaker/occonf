package occonf

import (
	"fmt"
	"reflect"
	"sort"
	"sync"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/ryanuber/go-glob"
)

// Validation flags.
const (
	CheckHasExtractor string = "has-extractor"
)

func CheckDefaults() map[string]bool {
	return map[string]bool{
		CheckHasExtractor: false,
	}
}

func NewConfig() *Config {
	var c Config
	c.init()
	return &c
}

type Config struct {
	initOnce sync.Once

	Warnings []error
	checks   map[string]bool

	IndexFields        map[string]IndexField
	ContentTypes       map[string]map[string]Property
	Extractors         map[string]Extractor
	XPathNamespaces    map[string]XPathNamespace
	XSLTExtractors     map[string]XSLTExtractor
	RelationExtractors map[string]RelationExtractor
	Groups             map[string][]Permission
	GroupRules         map[string]GroupRule
	Paths              map[string]PathPattern
	Relations          map[string]Relation
	ContentTypeRules   map[string]ContentTypeRule
	StorageRules       map[string]StorageRule
	Sorting            map[SortingKey]Sorting
	Versioning         *Versioning
	DateAndTime        *DateAndTime
	Faceting           *Faceting
}

type SortingKey struct {
	Name        string
	ContentType string
}

func (c *Config) init() {
	c.initOnce.Do(func() {
		c.checks = CheckDefaults()
		c.IndexFields = make(map[string]IndexField)
		c.ContentTypes = make(map[string]map[string]Property)
		c.Extractors = make(map[string]Extractor)
		c.XPathNamespaces = make(map[string]XPathNamespace)
		c.XSLTExtractors = make(map[string]XSLTExtractor)
		c.RelationExtractors = make(map[string]RelationExtractor)
		c.Groups = make(map[string][]Permission)
		c.GroupRules = make(map[string]GroupRule)
		c.Paths = make(map[string]PathPattern)
		c.Relations = make(map[string]Relation)
		c.ContentTypeRules = make(map[string]ContentTypeRule)
		c.StorageRules = make(map[string]StorageRule)
		c.Sorting = make(map[SortingKey]Sorting)
	})
}

func (c *Config) Validate(ignore []string) []error {
	var errs []error

	list := c.validate()

	for _, err := range list {
		if matchesAPattern(err.Error(), ignore) {
			continue
		}

		errs = append(errs, err)
	}

	return errs
}

func matchesAPattern(v string, patterns []string) bool {
	for _, p := range patterns {
		if glob.Glob("*"+p+"*", v) {
			return true
		}
	}

	return false
}

func (c *Config) validate() []error {
	var errs []error

	for _, v := range c.IndexFields {
		err := validateIntegrity(c, v)
		if err != nil {
			errs = append(errs, err)
		}
	}

	for _, props := range c.ContentTypes {
		for _, v := range props {
			err := validateIntegrity(c, v)
			if err != nil {
				errs = append(errs, err)
			}
		}
	}

	for _, ex := range c.Extractors {
		err := validateIntegrity(c, ex)
		if err != nil {
			errs = append(errs, err)
		}
	}

	for _, ex := range c.XSLTExtractors {
		err := validateIntegrity(c, ex)
		if err != nil {
			errs = append(errs, err)
		}
	}

	for _, ex := range c.RelationExtractors {
		err := validateIntegrity(c, ex)
		if err != nil {
			errs = append(errs, err)
		}
	}

	for _, r := range c.ContentTypeRules {
		err := validateIntegrity(c, r)
		if err != nil {
			errs = append(errs, err)
		}
	}

	for _, r := range c.StorageRules {
		err := validateIntegrity(c, r)
		if err != nil {
			errs = append(errs, err)
		}
	}

	err := c.Versioning.Validate(c)
	if err != nil {
		errs = append(errs, err)
	}

	definedExt := make(map[string]string)

	for _, ex := range c.Extractors {
		ident := fmt.Sprintf("%s.%s (%s)",
			ex.ContentType, ex.Property, ex.MimeType)
		desc := "xpath extractor with the id " + ex.ID

		existingDesc, exists := definedExt[ident]
		if exists {
			errs = append(errs, fmt.Errorf(
				"error when validating %s, %s has already been defined by %s",
				desc, ident, existingDesc))
		}

		definedExt[ident] = desc
	}

	for _, ex := range c.XSLTExtractors {
		ident := fmt.Sprintf("%s.%s (%s)",
			ex.ContentType, ex.Property, ex.MimeType)
		desc := "xslt extractor with the id " + ex.ID

		existingDesc, exists := definedExt[ident]
		if exists {
			errs = append(errs, fmt.Errorf(
				"error when validating %s, %s has already been defined by %s",
				desc, ident, existingDesc))
		}

		definedExt[ident] = desc
	}

	return errs
}

func (c *Config) WriteToFile(path string, format ConfigFormat) error {
	f, err := c.ToFile(format)
	if err != nil {
		return err
	}

	return f.WriteToFile(path)
}

func (c *Config) ToFile(format ConfigFormat) (ConfigFile, error) {
	var f ConfigFile

	for _, v := range c.IndexFields {
		f.IndexFields = append(f.IndexFields, v)
	}

	for t, props := range c.ContentTypes {
		var propList []Property

		for _, v := range props {
			propList = append(propList, v)
		}

		f.ContentTypes = append(f.ContentTypes, map[string][]Property{
			t: propList,
		})
	}

	for _, v := range c.XPathNamespaces {
		f.XPathNamespaces = append(f.XPathNamespaces, v)
	}

	for _, v := range c.Extractors {
		f.Extractors = append(f.Extractors, v)
	}

	for _, v := range c.XSLTExtractors {
		f.XSLTExtractors = append(f.XSLTExtractors, v)
	}

	for _, v := range c.RelationExtractors {
		err := v.Normalise(format)
		if err != nil {
			return f, fmt.Errorf("failed to adapt %s to the format %v",
				v.Identity(), format)
		}

		f.RelationExtractors = append(f.RelationExtractors, v)
	}

	for group, permissions := range c.Groups {
		f.Groups = append(f.Groups, group)
		f.Permissions = append(f.Permissions, permissions...)
	}

	for _, v := range c.GroupRules {
		f.GroupRules = append(f.GroupRules, v)
	}

	for _, v := range c.ContentTypeRules {
		f.ContentTypeRules = append(f.ContentTypeRules, v)
	}

	if format == FormatV2 {
		for _, v := range c.StorageRules {
			f.StorageRule = append(f.StorageRule, v)
		}

		if c.Versioning != nil {
			v := *c.Versioning
			f.Versioning = &v
		}

		for _, v := range c.Sorting {
			f.Sorting = append(f.Sorting, v)
		}

		if c.DateAndTime != nil {
			v := *c.DateAndTime
			f.DateAndTime = &v
		}

		for _, v := range c.Paths {
			f.Paths = append(f.Paths, v)
		}

		if c.Faceting != nil {
			v := *c.Faceting
			f.Faceting = &v
		}
	}

	f.Sort()

	return f, nil
}

func stringSliceUnion(a, b []string) []string {
	if a == nil && b == nil {
		return nil
	}

	u := make([]string, len(a))
	copy(u, a)

	for i := range b {
		var exists bool
		for j := range u {
			if u[j] == b[i] {
				exists = true
				break
			}
		}

		if exists {
			continue
		}

		u = append(u, b[i])
	}

	sort.Strings(u)

	return u
}

func validateIntegrity(c *Config, v Validatable) error {
	err := v.Validate(c)
	if err != nil {
		msg := "invalid " + v.Identity()
		if sp, ok := v.(sourcePath); ok {
			msg = fmt.Sprintf("%s from %q", msg, sp.SourcePath())
		}

		return fmt.Errorf("%s: %w",
			msg, err)
	}

	return nil
}

type ConflictResolver interface {
	Resolve(f ConfigFile, rErr *RedeclaredError) (Identifiable, error)
	Warnings() []error
}

type OverwriteWithWarning struct {
	warnings []error
}

func (r *OverwriteWithWarning) Resolve(f ConfigFile, rErr *RedeclaredError) (Identifiable, error) {
	r.warnings = append(r.warnings, fmt.Errorf("%s: %w", f.Path, rErr))

	return rErr.New, nil
}

func (r *OverwriteWithWarning) Warnings() []error {
	return r.warnings
}

type resolverTypeChecker struct {
	resolver ConflictResolver
}

func (r *resolverTypeChecker) Resolve(f ConfigFile, rErr *RedeclaredError) (Identifiable, error) {
	t := reflect.TypeOf(rErr.Old)

	winner, err := r.resolver.Resolve(f, rErr)
	if err != nil {
		return rErr.Old, err
	}

	wt := reflect.TypeOf(winner)
	if !wt.AssignableTo(t) {
		return rErr.Old, fmt.Errorf("expected resolved value to be assignable to %s, got %s",
			t.Name(), wt.Name())
	}

	return winner, nil
}

func (r *resolverTypeChecker) Warnings() []error {
	return r.resolver.Warnings()
}

func (c *Config) Merge(f ConfigFile, resolv ConflictResolver) error {
	c.init()

	resolv = &resolverTypeChecker{
		resolver: resolv,
	}

	for i, field := range f.IndexFields {
		field.File = f.Path

		err := validate(i, field)
		if err != nil {
			return err
		}

		ef, exists := c.IndexFields[field.Name]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, ef, field))
			if err != nil {
				return err
			}

			field = winner.(IndexField)
		}

		c.IndexFields[field.Name] = field
	}

	for _, types := range f.ContentTypes {
		for name, properties := range types {
			err := c.mergeProperties(f, name, properties, resolv)
			if err != nil {
				return fmt.Errorf(
					"failed to merge properties for %q: %w",
					name, err)
			}
		}
	}

	for i, ex := range f.Extractors {
		ex.File = f.Path

		err := validate(i, ex)
		if err != nil {
			return err
		}

		eEx, exists := c.Extractors[ex.ID]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, eEx, ex))
			if err != nil {
				return err
			}

			ex = winner.(Extractor)
		}

		c.Extractors[ex.ID] = ex
	}

	for _, ex := range f.XSLTExtractors {
		ex.File = f.Path

		eEx, exists := c.XSLTExtractors[ex.ID]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, eEx, ex))
			if err != nil {
				return err
			}

			ex = winner.(XSLTExtractor)
		}

		c.XSLTExtractors[ex.ID] = ex
	}

	for _, ex := range f.RelationExtractors {
		ex.File = f.Path

		err := ex.Normalise(FormatV3)
		if err != nil {
			return fmt.Errorf("failed to normalise %s: %w",
				ex.Identity(), err)
		}

		eEx, exists := c.RelationExtractors[ex.ID]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, eEx, ex))
			if err != nil {
				return err
			}

			ex = winner.(RelationExtractor)
		}

		c.RelationExtractors[ex.ID] = ex
	}

	for _, group := range f.Groups {
		_, exists := c.Groups[group]
		if !exists {
			c.Groups[group] = nil
		}
	}

	for _, rule := range f.GroupRules {
		rule.File = f.Path

		rEx, exists := c.GroupRules[rule.ID]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, rEx, rule))
			if err != nil {
				return err
			}

			rule = winner.(GroupRule)
		}

		c.GroupRules[rule.ID] = rule
	}

	for _, rel := range f.Relations {
		rel.File = f.Path

		rEx, exists := c.Relations[rel.Name]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, rEx, rel))
			if err != nil {
				return err
			}

			rel = winner.(Relation)
		}

		c.Relations[rel.Name] = rel
	}

	for _, permission := range f.Permissions {
		permission.File = f.Path

		var perm []Permission
		var conflict bool

		ex := c.Groups[permission.Group]
		for _, exP := range ex {
			if exP.Role != permission.Role {
				perm = append(perm, exP)
				continue
			}

			winner, err := resolv.Resolve(f, redeclaredError(f.Path, exP, permission))
			if err != nil {
				return err
			}

			permission = winner.(Permission)

			perm = append(perm, permission)
			conflict = true
		}

		if !conflict {
			perm = append(perm, permission)
		}

		c.Groups[permission.Group] = perm
	}

	for i, rule := range f.ContentTypeRules {
		rule.File = f.Path

		err := validate(i, rule)
		if err != nil {
			return err
		}

		rEx, exists := c.ContentTypeRules[rule.ID]
		if exists {
			sort.Strings(rEx.MimeTypes)
			sort.Strings(rEx.MetadataMimeTypes)

			rule.MetadataMimeTypes = stringSliceUnion(
				rule.MetadataMimeTypes,
				rEx.MetadataMimeTypes,
			)

			rule.MimeTypes = stringSliceUnion(
				rule.MimeTypes,
				rEx.MimeTypes,
			)

			winner, err := resolv.Resolve(f, redeclaredError(f.Path, rEx, rule))
			if err != nil {
				return err
			}

			rule = winner.(ContentTypeRule)
		}

		c.ContentTypeRules[rule.ID] = rule
	}

	for i, rule := range f.StorageRule {
		rule.File = f.Path

		err := validate(i, rule)
		if err != nil {
			return err
		}

		rEx, exists := c.ContentTypeRules[rule.ID]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, rEx, rule))
			if err != nil {
				return err
			}

			rule = winner.(StorageRule)
		}

		c.StorageRules[rule.ID] = rule
	}

	if f.Versioning != nil {
		err := c.mergeVersioning(f, resolv)
		if err != nil {
			return err
		}
	}

	for _, sort := range f.Sorting {
		sort.File = f.Path

		key := SortingKey{
			Name:        sort.Name,
			ContentType: sort.ContentType,
		}

		sEx, exists := c.Sorting[key]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, sEx, sort))
			if err != nil {
				return err
			}

			sort = winner.(Sorting)
		}

		c.Sorting[key] = sort
	}

	if f.DateAndTime != nil {
		f.DateAndTime.File = f.Path

		dt := f.DateAndTime

		if c.DateAndTime != nil {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, c.DateAndTime, f.DateAndTime))
			if err != nil {
				return err
			}

			dt = winner.(*DateAndTime)
		}

		c.DateAndTime = dt
	}

	for _, ns := range f.XPathNamespaces {
		ns.File = f.Path

		nsEx, exists := c.XPathNamespaces[ns.Prefix]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, nsEx, ns))
			if err != nil {
				return err
			}

			ns = winner.(XPathNamespace)
		}

		c.XPathNamespaces[ns.Prefix] = ns
	}

	for _, p := range f.Paths {
		p.File = f.Path

		pEx, exists := c.Paths[p.ContentType]
		if exists {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, pEx, p))
			if err != nil {
				return err
			}

			p = winner.(PathPattern)
		}

		c.Paths[p.ContentType] = p
	}

	if f.Faceting != nil {
		f.Faceting.File = f.Path

		facets := f.Faceting

		if c.Faceting != nil {
			winner, err := resolv.Resolve(f, redeclaredError(f.Path, c.Faceting, f.Faceting))
			if err != nil {
				return err
			}

			facets = winner.(*Faceting)
		}

		c.Faceting = facets
	}

	return nil
}

func (c *Config) mergeVersioning(f ConfigFile, resolv ConflictResolver) error {
	if c.Versioning == nil {
		c.Versioning = f.Versioning
		return nil
	}

	v := f.Versioning
	current := c.Versioning

	if v.KeepOnDelete != nil {
		keep := *v.KeepOnDelete
		c.Versioning.KeepOnDelete = &keep
	}

	for _, cr := range v.ContentTypes {
		cr.File = f.Path

		exCr, idx := current.GetRule(cr.Name)
		if idx < 0 {
			current.ContentTypes = append(current.ContentTypes, cr)
			continue
		}

		winner, err := resolv.Resolve(f, redeclaredError(f.Path, exCr, cr))
		if err != nil {
			return err
		}

		cr = winner.(ContentTypeVersioning)

		c.warningInFile(v.File, err)
		current.ContentTypes[idx] = cr
	}

	return nil
}

type Validatable interface {
	Identifiable
	Validate(conf *Config) error
}

func validate(idx int, v Validatable) error {
	err := v.Validate(nil)
	if err != nil {
		return fmt.Errorf("invalid %s, pos %d: %w",
			v.Identity(), idx, err)
	}

	return nil
}

type Identifiable interface {
	Identity() string
}

type RedeclaredError struct {
	Diff       string
	FullDiff   string
	Identity   string
	File       string
	DeclaredIn string
	Old        Identifiable
	New        Identifiable
}

func (err *RedeclaredError) Error() string {
	msg := err.Identity + " was redeclared"

	if err.Diff == "" {
		return fmt.Sprintf("%s, but was unchanged", msg)
	}

	return fmt.Sprintf("%s, diff:\n%s", msg, err.FullDiff)
}

type sourcePath interface {
	SourcePath() string
}

func concrete(v interface{}) interface{} {
	rV := reflect.ValueOf(v)
	if rV.Kind() == reflect.Ptr {
		return rV.Elem().Interface()
	}
	return v
}

func redeclaredError(file string, old, new Identifiable) *RedeclaredError {
	value := concrete(old)
	sansFileOpt := cmpopts.IgnoreFields(value, "File")

	err := RedeclaredError{
		Diff:     cmp.Diff(old, new, sansFileOpt),
		FullDiff: cmp.Diff(old, new),
		Identity: old.Identity(),
		File:     file,
		Old:      old,
		New:      new,
	}

	declaredIn, ok := old.(sourcePath)
	if ok {
		err.DeclaredIn = declaredIn.SourcePath()
	}

	return &err
}

func (c *Config) warningInFile(file string, err error) {
	c.Warnings = append(c.Warnings, fmt.Errorf("%s: %w", file, err))
}

func (c *Config) mergeProperties(
	file ConfigFile,
	contentType string, properties []Property, resolv ConflictResolver,
) error {
	props, ok := c.ContentTypes[contentType]
	if !ok {
		props = make(map[string]Property)
		c.ContentTypes[contentType] = props
	}

	for i, prop := range properties {
		prop.File = file.Path
		prop.ContentType = contentType

		err := validate(i, prop)
		if err != nil {
			return err
		}

		ep, exists := props[prop.Name]
		if exists {
			winner, err := resolv.Resolve(file, redeclaredError(file.Path, ep, prop))
			if err != nil {
				return err
			}

			prop = winner.(Property)
		}

		props[prop.Name] = prop
	}

	return nil
}
