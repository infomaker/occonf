package occonf

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"bitbucket.org/infomaker/clisess"
	"github.com/aws/aws-sdk-go/service/s3"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/goccy/go-yaml"
	"github.com/urfave/cli/v2"
)

func Run() error {
	var checks []string
	checkDefaults := CheckDefaults()

	for k := range checkDefaults {
		checks = append(checks, k)
	}

	app := cli.App{

		Commands: []*cli.Command{
			{
				Name:        "merge",
				Action:      mergeCommand,
				Usage:       "merges open content configurations",
				Description: "takes one or more configuration files and merges and validates them",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "format",
						Value: string(FormatV2),
						Usage: `The format to use, one of "oc2" or "oc3"`,
					},
					&cli.StringFlag{
						Name:  "out",
						Usage: `The output file, defaults to stdout`,
					},
					&cli.StringFlag{
						Name:  "strategy",
						Value: "overwrite",
						Usage: `The merge strategy to use, one of "overwrite" and "interactive"`,
					},
					&cli.StringSliceFlag{
						Name:    "auto-merge",
						Aliases: []string{"a"},
						Usage:   `Automatically apply changes for specific fields when running interactively`,
					},
					&cli.StringSliceFlag{
						Name:    "disable",
						Aliases: []string{"d"},
						Usage: fmt.Sprintf(
							`Disable configuration checks, can be specified multiple times, one of: %s`,
							strings.Join(checks, ", "),
						),
					},
					&cli.StringSliceFlag{
						Name:    "enable",
						Aliases: []string{"e"},
						Usage: fmt.Sprintf(
							`Enable configuration checks, can be specified multiple times, one of: %s`,
							strings.Join(checks, ", "),
						),
					},
					&cli.BoolFlag{
						Name:  "strict",
						Value: true,
						Usage: `Treat warnings as errors`,
					},
					&cli.StringSliceFlag{
						Name:  "ignore",
						Usage: `Ignore matching validation errors`,
					},
				},
			},
			{
				Name:        "validate",
				Action:      validateCommand,
				Usage:       "validates open content configurations",
				Description: "takes one or more configuration files and merges and validates them",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "format",
						Value: string(FormatV2),
						Usage: `The format to use, one of "oc2" or "oc3"`,
					},
					&cli.StringSliceFlag{
						Name:    "auto-merge",
						Aliases: []string{"a"},
						Usage:   `Automatically apply changes for specific fields when running interactively`,
					},
					&cli.StringSliceFlag{
						Name:    "disable",
						Aliases: []string{"d"},
						Usage: fmt.Sprintf(
							`Disable configuration checks, can be specified multiple times, one of: %s`,
							strings.Join(checks, ", "),
						),
					},
					&cli.StringSliceFlag{
						Name:    "enable",
						Aliases: []string{"e"},
						Usage: fmt.Sprintf(
							`Enable configuration checks, can be specified multiple times, one of: %s`,
							strings.Join(checks, ", "),
						),
					},
					&cli.BoolFlag{
						Name:  "strict",
						Value: true,
						Usage: `Treat warnings as errors`,
					},
					&cli.StringSliceFlag{
						Name:  "ignore",
						Usage: `Ignore matching validation errors`,
					},
				},
			},
			{
				Name:   "download",
				Action: downloadCommand,
				Usage:  "downloads open content configurations",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "role",
						Value: "editorial",
						Usage: `The OC role the config is for, one of "editorial" or "public"`,
					},
					&cli.StringFlag{
						Name:    "deployment",
						Aliases: []string{"d"},
						Value:   "production",
						Usage:   `The deployment to read from, normally one of "test", "staging", or "production"`,
					},
					&cli.StringFlag{
						Name:    "version",
						Aliases: []string{"v"},
						Usage:   `The version to download defaults to latest`,
					},
					&cli.BoolFlag{
						Name:    "pre-release",
						Aliases: []string{"p"},
						Usage:   `Use pre-releases`,
					},
					&cli.StringFlag{
						Name:    "out",
						Aliases: []string{"o"},
						Usage:   `output file path, defaults to a role and version name`,
					},
				},
			},
		},
	}

	return app.Run(os.Args)
}

func downloadCommand(ctx *cli.Context) error {
	role := ctx.String("role")
	deployment := ctx.String("deployment")
	version := ctx.String("version")
	out := ctx.String("out")
	pre := ctx.Bool("pre-release")

	sess, err := clisess.AssumeProfile(os.Getenv("AWS_PROFILE"))
	if err != nil {
		return err
	}

	bucket := "im-saas-build-artifacts"

	s3Svc := s3.New(sess)

	if version == "" || version == "latest" {
		latestKey := fmt.Sprintf("/opencontent-config/%s/latest", deployment)
		if pre {
			latestKey += "-prerelease"
		}

		obj, err := s3Svc.GetObject(&s3.GetObjectInput{
			Bucket: &bucket,
			Key:    &latestKey,
		})
		if err != nil {
			return fmt.Errorf("could not get latest version: %w", err)
		}
		defer obj.Body.Close()

		latestVers, err := io.ReadAll(obj.Body)
		if err != nil {
			return fmt.Errorf("failed to read latest version: %w", err)
		}

		latestVers = bytes.TrimSpace(latestVers)
		version = string(latestVers)
		println("latest version is ", version)
	}

	confKey := fmt.Sprintf("/opencontent-config/%s/%s/%s.yaml",
		deployment, version, role)

	confObj, err := s3Svc.GetObject(&s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &confKey,
	})
	if err != nil {
		return fmt.Errorf("could not get fetch configuration: %w", err)
	}
	defer confObj.Body.Close()

	if out == "" {
		out = fmt.Sprintf("%s-%s.yaml", role, version)
		println("writing config to", out)
	}

	f, err := os.Create(out)
	if err != nil {
		return fmt.Errorf("failed to create output file %s: %w", out, err)
	}
	defer f.Close()

	_, err = io.Copy(f, confObj.Body)
	if err != nil {
		return fmt.Errorf("failed to write config to file: %w", err)
	}

	return nil
}

type mergeInvokeParams struct {
	Format    string
	Strategy  string
	Strict    bool
	Validate  bool
	Ignore    []string
	Paths     []string
	AutoMerge []string
	Checks    map[string]bool
	OutPath   string
}

func validateCommand(ctx *cli.Context) error {
	p := mergeInvokeParams{
		Format:   ctx.String("format"),
		Strategy: "overwrite",
		Strict:   ctx.Bool("strict"),
		Validate: true,
		Ignore:   ctx.StringSlice("ignore"),
		Checks: checksFromEnableDisable(
			ctx.StringSlice("enable"),
			ctx.StringSlice("disable"),
		),
		Paths:     ctx.Args().Slice(),
		AutoMerge: ctx.StringSlice("auto-merge"),
	}

	return doMerge(p)
}

func mergeCommand(ctx *cli.Context) error {
	p := mergeInvokeParams{
		Format:   ctx.String("format"),
		Strategy: ctx.String("strategy"),
		Strict:   ctx.Bool("strict"),
		Checks: checksFromEnableDisable(
			ctx.StringSlice("enable"),
			ctx.StringSlice("disable"),
		),
		Ignore:    ctx.StringSlice("ignore"),
		OutPath:   ctx.String("out"),
		Paths:     ctx.Args().Slice(),
		AutoMerge: ctx.StringSlice("auto-merge"),
	}

	return doMerge(p)
}

func checksFromEnableDisable(enable, disable []string) map[string]bool {
	c := make(map[string]bool)

	for _, k := range enable {
		c[k] = true
	}

	for _, k := range disable {
		c[k] = false
	}

	return c
}

func doMerge(p mergeInvokeParams) error {
	if len(p.Paths) == 0 {
		return errors.New("supply at least one file as an argument")
	}

	if p.Validate && p.OutPath != "" {
		return errors.New("an out path cannot be specified when validating")
	}

	formats := map[string]ConfigFormat{
		string(FormatV2): FormatV2,
		string(FormatV3): FormatV3,
	}

	configFormat, ok := formats[p.Format]
	if !ok {
		return fmt.Errorf("unknown config format %q", p.Format)
	}

	opts := []MergeOption{
		WithMergeOutputFormat(configFormat),
		WithChecks(p.Checks),
	}

	var err error
	var res *Config
	var resolver ConflictResolver

	switch p.Strategy {
	case "overwrite":
		resolver = &OverwriteWithWarning{}
		opts = append(opts, WithMergeResolver(resolver))

		res, err = MergeFiles(p.Paths, opts...)
	case "interactive":
		res, err = interactiveMerge(p.Paths, p.AutoMerge, opts)
	default:
		return fmt.Errorf("unknown merge strategy %q", p.Strategy)
	}

	if err != nil {
		return fmt.Errorf("failed to merge files: %w", err)
	}

	if res == nil {
		return errors.New("merge failed (aborted)")
	}

	var writer io.Writer = os.Stdout

	if p.Validate {
		writer = io.Discard
	} else if p.OutPath != "" {
		f, err := os.Create(p.OutPath)
		if err != nil {
			return fmt.Errorf("failed to create output file %q: %w",
				p.OutPath, err)
		}

		defer f.Close()

		writer = f
	}

	outFile, err := res.ToFile(configFormat)
	if err != nil {
		return fmt.Errorf("failed to create output file: %w", err)
	}

	err = yaml.NewEncoder(writer).Encode(outFile)
	if err != nil {
		return fmt.Errorf("failed to encode merged config: %w", err)
	}

	errs := res.Validate(p.Ignore)
	if len(errs) > 0 {
		messages := make([]string, len(errs))
		for i := range errs {
			messages[i] = errs[i].Error()
		}

		return fmt.Errorf("integrity check failed:\n%s",
			strings.Join(messages, "\n"))
	}

	// Dumb display of warnings
	if resolver != nil {
		rWarnings := resolver.Warnings()
		var errs []error
		var warnings []error

		for _, err := range rWarnings {
			var rErr *RedeclaredError
			if errors.As(err, &rErr) {
				if rErr.Diff == "" {
					warnings = append(warnings, err)
					continue
				}
			}

			errs = append(errs, err)
		}

		for _, err := range errs {
			println("error:", err.Error())
		}

		for _, err := range warnings {
			println("warning:", err.Error())
		}

		if len(errs) > 0 || (p.Strict && len(warnings) > 0) {
			return fmt.Errorf(
				"the merge process generated %d errors and %d warnings",
				len(errs), len(warnings),
			)
		}

		fmt.Printf(
			"the merge process generated %d errors and %d warnings\n",
			len(errs), len(warnings),
		)
	}

	return nil
}

func interactiveMerge(
	paths []string, auto []string, options []MergeOption,
) (*Config, error) {
	sess := NewMergeSession(auto...)

	fail := make(chan error)
	exit := make(chan struct{})

	p := tea.NewProgram(sess)

	go func() {
		err := p.Start()
		if err != nil {
			fail <- err
		}
		close(exit)
	}()

	var mergedFile *Config

	go func() {
		defer sess.Done()

		var opts []MergeOption

		opts = append(opts, options...)
		opts = append(opts, WithMergeResolver(sess))

		res, err := MergeFiles(paths, opts...)
		if err != nil {
			fail <- err
			return
		}

		mergedFile = res
	}()

	select {
	case err := <-fail:
		// Let the application clean up terminal state before exit
		<-exit

		return nil, err
	case <-exit:
		return mergedFile, nil
	}
}
