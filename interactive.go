package occonf

import (
	"reflect"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/muesli/termenv"
)

type conflictResolution struct {
	Result Identifiable
	Err    error
}

type MergeSession struct {
	conflicts   chan *RedeclaredError
	resolutions chan conflictResolution

	autoAccept []string

	current *RedeclaredError
}

func NewMergeSession(autoAccept ...string) *MergeSession {
	m := MergeSession{
		conflicts:   make(chan *RedeclaredError),
		resolutions: make(chan conflictResolution),
		autoAccept:  append(autoAccept, "File"),
	}

	return &m
}

func (m *MergeSession) Resolve(f ConfigFile, rErr *RedeclaredError) (Identifiable, error) {
	// If we don't have a real difference
	if rErr.Diff == "" {
		return rErr.New, nil
	}

	if len(m.autoAccept) > 0 {
		old := concrete(rErr.Old)

		var opts cmp.Options
		for _, field := range m.autoAccept {
			f := reflect.ValueOf(old).FieldByName(field)
			if !f.IsValid() {
				continue
			}

			opts = append(opts, cmpopts.IgnoreFields(old, field))
		}

		diff := cmp.Diff(rErr.Old, rErr.New, opts...)
		if diff == "" {
			return rErr.New, nil
		}
	}

	m.conflicts <- rErr

	res := <-m.resolutions

	return res.Result, res.Err
}

func (m *MergeSession) Warnings() []error {
	return nil
}

func (m *MergeSession) Done() {
	close(m.conflicts)
}

func (m *MergeSession) Init() tea.Cmd {
	return getNextConflict
}

func (m *MergeSession) awaitNext() bool {
	if m.current != nil {
		return true
	}

	c, ok := <-m.conflicts
	if !ok {
		return false
	}
	m.current = c

	return true
}

func getNextConflict() tea.Msg {
	return nextConflict{}
}

type nextConflict struct{}

func (m *MergeSession) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case nextConflict:
		if !m.awaitNext() {
			return m, tea.Quit
		}

	// Is it a key press?
	case tea.KeyMsg:

		// Cool, what was the actual key pressed?
		switch msg.String() {
		// These keys should exit the program.
		case "ctrl+c", "q":
			return m, tea.Quit

		// The "up" and "k" keys keeps the previous value.
		case "up", "k":
			m.resolutions <- conflictResolution{
				Result: m.current.Old,
			}
			m.current = nil

			return m, getNextConflict

		// The "down" and "j" keys uses the new value
		case "down", "j":
			m.resolutions <- conflictResolution{
				Result: m.current.New,
			}
			m.current = nil

			return m, getNextConflict
		}
	}

	return m, nil
}

func (m *MergeSession) View() string {
	if m.current == nil {
		return ""
	}

	// The header
	s := termenv.String("Which version should we keep?\n\n").Foreground(termenv.ANSICyan).Bold().String()

	lines := strings.Split(m.current.FullDiff, "\n")

	for _, line := range lines {
		if line == "" {
			s += "\n"
			continue
		}

		switch line[0] {
		case '+':
			s += termenv.String(line).Foreground(termenv.ANSIGreen).String()
			s += "\n"
		case '-':
			s += termenv.String(line).Foreground(termenv.ANSIRed).String()
			s += "\n"
		default:
			s += line + "\n"
		}
	}

	// The footer
	s += termenv.String("\nPress up/k to keep old value, down/j to replace. Press q to quit.\n").Bold().String()

	// Send the UI for rendering
	return s
}
